import React, {useEffect, useState} from "react";
import {format} from "date-fns";
import {
    Toolbar,
    AppBar,
    Link,
    Typography,
    Accordion,
    AccordionDetails,
    AccordionSummary,
    CssBaseline,
    Container,
    Box,
} from "@material-ui/core";
import {AccountBalance, ExpandMore} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core/styles";

const url = "https://polar-scrubland-52638.herokuapp.com/api/transaction";

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {"Copyright © "}
            <Link color="inherit" href="">
                Accounting notebook
            </Link>{" "}
            {new Date().getFullYear()}
            {"."}
        </Typography>
    );
}

export default function App() {
    const classes = useStyles();

    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [transactions, setTransactions] = useState([]);

    useEffect(() => {
        let isCancelled = false;
        const fetchData = async () => {
            setLoading(true);
            try {
                const data = await fetch(url);
                if (!isCancelled) {
                    if (data.ok) {
                        const transactions = await data.json();
                        setTransactions(transactions);
                    }
                    setLoading(false);
                }
            } catch (e) {
                if (!isCancelled) {
                    setLoading(false);
                    setError(true);
                }
            }
        };

        fetchData();
        return () => (isCancelled = true);
    }, []);

    return (
        <>
            <CssBaseline/>
            <AppBar position="relative">
                <Toolbar>
                    <AccountBalance className={classes.icon}/>
                    <Typography variant="h6" color="inherit" noWrap>
                        Accounting Notebook
                    </Typography>
                </Toolbar>
            </AppBar>
            <main>
                <div className={classes.heroContent}>
                    <Container maxWidth="md">
                        <Typography
                            component="h2"
                            variant="h3"
                            align="left"
                            color="textSecondary"
                            gutterBottom
                        >
                            Transactions
                        </Typography>
                    </Container>

                    <Container maxWidth="md">
                        {error && <Typography>An error have ocurred</Typography>}
                        {loading ? (
                            <Typography>Loading...</Typography>
                        ) : transactions.length ? (
                            transactions.map(({id, type, amount, effectiveDate}) => (
                                <Accordion key={id}>
                                    <AccordionSummary
                                        expandIcon={<ExpandMore/>}
                                        style={
                                            type === "debit"
                                                ? {background: "red"}
                                                : {background: "green"}
                                        }
                                    >
                                        <Container className={classes.card}>
                                            <Typography className={classes.heading}>
                                                {type.toLocaleUpperCase()}
                                            </Typography>
                                            <Typography className={classes.heading}>
                                                {`$${amount.toFixed(2)}`}
                                            </Typography>
                                        </Container>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Box>
                                            <Typography paragraph>Transaction ID: {id}</Typography>
                                            <Typography>
                                                Transaction date:{" "}
                                                {format(new Date(effectiveDate), "yyyy-MM-dd")}
                                            </Typography>
                                        </Box>
                                    </AccordionDetails>
                                </Accordion>
                            ))
                        ) : (<Typography> No transactions found </Typography>)}
                    </Container>
                </div>
            </main>
            <footer>
                <Copyright/>
            </footer>
        </>
    );
}
