# Accounting notebook

This is my implementation of the proposed AgileEngine challenge.
It consists of a backend developed in NodeJs with ExpressJs as framework, 
deployed in [Heroku](https://polar-scrubland-52638.herokuapp.com) and on a client, based on CRA, deployed on [Now](https://account-notebook-front.nachodezan.vercel.app/).

#Design decisions
- Material UI was used to make the application more responsive, adapting it to many devices.  
- Function components and hooks were used instead of Class components, according to React [guidelines](https://reactjs.org/docs/hooks-intro.html#motivation).

## Run
In the project directory, you can run:

     npm install
     npm start

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.





 
